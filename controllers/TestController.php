<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;

class TestController extends Controller{

	public function actionFootballers_update(){
		
	}


public function actionTeam_create(){
	Yii::$app->response->format=\yii\web\Response::FORMAT_JSON;

	if(Yii::$app->request->isAjax){
		$data=Yii::$app->request->post();

		$team=new \app\models\FootballersTeams();
		$team->title=$data['title'];
		$team->save();
		$data['id']=$team->id;

		return(['data'=>$data,'error'=>null]);
	}else{
		return([
			'data'=>null,
			'error'=>'Only AJAX supported'
		]);
	}
}

public function actionFootballer_create()
{
    $model = new \app\models\Footballers();

    if ($model->load(Yii::$app->request->post())) {
        if ($model->validate()) {
			$model->save();
            return($this->render('footballer_create',[
				'model'=>new \app\models\Footballers(),
			]));
        }
    }
    return $this->render('footballer_create', [
        'model' => $model,
	]);
}

	public function actionIndex(){
		return($this->render('index'));
	}

}
