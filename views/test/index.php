<?php

$this->title="Test, view page";

use \yii\grid\GridView;
use \yii\data\ActiveDataProvider;
use \yii\widgets\Pjax;

$sm=new \app\models\Footballers();
$dp=new ActiveDataProvider([
	'query'=>$sm::find(),
]);

?>

<?php Pjax::begin(['id'=>'footballers']);?>
<?= GridView::widget([
	'dataProvider'=>$dp,
	'columns'=>[
		'id',
		'fName',
		'lName',
		'gender',
		'country',
		'team',
	],
]);?>
<?php Pjax::end();?>

<?php $this->registerJs('$(document).ready(function(){
	setInterval(function(){
		$.pjax.reload({container:"#footballers"});
	},1000);
});');?>