<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Footballers;

use conquer\select2\Select2Widget;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Footballers */
/* @var $form ActiveForm */
?>
<div class="test-footballer_create">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'fName') ?>
        <?= $form->field($model, 'lName') ?>
        <?= $form->field($model, 'bday')->widget(yii\jui\DatePicker::classname(),[
			'dateFormat'=>'yyyy-MM-dd'
		]) ?>
        <?= $form->field($model, 'country_id')->dropDownList(Footballers::$countries); ?>

		<?=$form->field($model,'team_id')->widget(Select2Widget::className(),[
			'items'=>ArrayHelper::map(\app\models\FootballersTeams::find()->all(),'id','title'),
			'settings'=>[
				'tags'=>true,
				'createTag'=>'js:function(p){
					return({
						id:0,
						text:p.term,
						newTag:false
					});
				}',
			],
			'events'=>[
				'select2:select'=>"function(e){
					if($(this).val()==0){
						var teams=$(this);
						$.ajax({
							type:'POST',
							url:'".\yii\helpers\BaseUrl::to(['test/team_create'])."',
							data:{
								title:e.params.data.text
							}
						}).done(function(data){
							console.log(data);
							var o=new Option(data.data.title,data.data.id,true,true);
							teams.append(o);
							return(true);
						}).fail(function(){
							console.error('AJAX error');
						});
						return(false);
					}
				}",
				//'select2:createTag'=>"function(d){console.log(d);}",
			],
		]);?>

		<?= $form->field($model, 'gender_id')->dropdownList(Footballers::$genders,[]); ?>

        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- test-footballer_create -->

<?php
$s= <<< TEAMJS
$('#footballers-team_id').select2({
	createTag:function(d){
		console.log(d);
	}
});
TEAMJS;

$this->registerJs($s,\yii\web\View::POS_END);
?>
