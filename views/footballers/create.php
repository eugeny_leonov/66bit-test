<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Footballers */

$this->title = 'Create Footballers';
$this->params['breadcrumbs'][] = ['label' => 'Footballers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="footballers-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
