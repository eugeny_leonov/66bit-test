<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FootballersTeams */

$this->title = 'Create Footballers Teams';
$this->params['breadcrumbs'][] = ['label' => 'Footballers Teams', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="footballers-teams-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
