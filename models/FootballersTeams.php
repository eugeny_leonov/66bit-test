<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "footballers_teams".
 *
 * @property int $id
 * @property string $title
 */
class FootballersTeams extends \yii\db\ActiveRecord
{

	public function createTeam($title){

	}

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'footballers_teams';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Team title',
        ];
    }
}
