<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "footballers".
 *
 * @property int $id
 * @property string $fName
 * @property string $lName
 * @property int $gender_id
 * @property string $bday
 * @property int $country_id
 * @property int $team_id
 */
class Footballers extends \yii\db\ActiveRecord
{

	public static $countries=[
		null=>'Выбрать страну',
		1=>'Россия',
		2=>'США',
		3=>'Италия',
	];

	public static $genders=[
		1=>'Мужской',
		2=>'Женский',
	];

	public function getCountry(){
		return(self::$countries[$this->country_id]);
	}

	public function getGender(){
		return(self::$genders[$this->gender_id]);
	}

	public function getTeam(){
		$ft=new FootballersTeams();
		return($ft->findOne(['id'=>$this->team_id])->title);
	}

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'footballers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fName', 'lName', 'bday','country_id', 'team_id'], 'required'],
			[['gender_id', 'country_id', 'team_id'], 'integer'],
			//[['country_id'],'required','isEmpty'=>function($val){return(is_int($val)&&($val>1));}],
            [['bday'], 'safe'],
            [['fName', 'lName'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fName' => 'Имя',
            'lName' => 'Фамилия',
			'gender_id' => 'Пол',
			'gender'=>'Пол',
            'bday' => 'Дата рождения',
			'country_id' => 'Страна',
			'country'=>'Страна',
			'team_id' => 'Команда',
			'team'=>'Команда'
        ];
    }
}
