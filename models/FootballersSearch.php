<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Footballers;

/**
 * FootballersSearch represents the model behind the search form of `app\models\Footballers`.
 */
class FootballersSearch extends Footballers
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'gender_id', 'country_id', 'team_id'], 'integer'],
            [['fName', 'lName', 'bday'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Footballers::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'gender_id' => $this->gender_id,
            'bday' => $this->bday,
            'country_id' => $this->country_id,
            'team_id' => $this->team_id,
        ]);

        $query->andFilterWhere(['like', 'fName', $this->fName])
            ->andFilterWhere(['like', 'lName', $this->lName]);

        return $dataProvider;
    }
}
