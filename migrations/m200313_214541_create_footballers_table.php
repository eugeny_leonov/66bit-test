<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%footballers}}`.
 */
class m200313_214541_create_footballers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%footballers}}', [
			'id' => $this->primaryKey(),
			'fName'=>$this->string()->notNull(),
			'lName'=>$this->string()->notNull(),
			'gender_id'=>$this->tinyInteger()->notNull()->defaultValue(1),
			'bday'=>$this->date()->notNull(),
			'country_id'=>$this->integer()->notNull(),
			'team_id'=>$this->integer()->notNull(),
		]);
		
		$this->createTable('{{%footballers_teams}}',[
			'id'=>$this->primaryKey(),
			'title'=>$this->string()->notNull(),
		]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
		$this->dropTable('{{%footballers}}');
		$this->dropTable('{{%footballers_teams');
    }
}
