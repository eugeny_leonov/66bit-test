<?php

use yii\db\Migration;

/**
 * Class m200315_213132_alter_tables
 */
class m200315_213132_alter_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->alterColumn('{{%footballers%}}', 'fName', $this->string()->append('CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL'));
		$this->alterColumn('{{%footballers%}}', 'lName', $this->string()->append('CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL'));
		$this->alterColumn('{{%footballers_teams%}}', 'title', $this->string()->append('CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL'));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200315_213132_alter_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200315_213132_alter_tables cannot be reverted.\n";

        return false;
    }
    */
}
